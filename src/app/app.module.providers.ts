import { ENV_PROVIDERS } from './environment'

import { 
	StorageProvider,
	AppState,
	LoginService
} from './core-services'


import { 
	LoginGuard
} from './guards'

// Application wide providers
const APP_PROVIDERS = [
  AppState,
  StorageProvider,
  LoginService,
  LoginGuard
]

export const PROVIDERS = [ // expose our Services and Providers into Angular's dependency injection
  ENV_PROVIDERS,
  APP_PROVIDERS
]
