import { Component, Injector } from '@angular/core'
import { forwardRef } from '@angular/core'

import { NG_VALUE_ACCESSOR } from '@angular/forms'
import { NG_VALIDATORS } from '@angular/forms'

import { InputBaseComponent } from '../input-base/input-base.component'


@Component({
  selector: 'inputs__email',
  templateUrl: '../input-base/input-base.component.pug',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line
      useExisting: forwardRef(() => EmailInputComponent),
        multi: true
    },
    // {
    //   provide: NG_VALIDATORS,
    //   useValue: validate,
    //   multi: true
    // }
  ]
})

export class EmailInputComponent extends InputBaseComponent {
  public type: string = 'email'
  public label: string = 'E-mail'
  public placeholder: string = 'E-mail'
  constructor(injector: Injector) { super(injector) }
}
