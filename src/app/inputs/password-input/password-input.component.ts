import { Component, Injector } from '@angular/core'
import { forwardRef } from '@angular/core'

import { NG_VALUE_ACCESSOR } from '@angular/forms'
import { NG_VALIDATORS } from '@angular/forms'

import { InputBaseComponent } from '../input-base/input-base.component'


@Component({
  selector: 'inputs__password',
  templateUrl: '../input-base/input-base.component.pug',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line
      useExisting: forwardRef(() => PasswordInputComponent),
        multi: true
    },
    // {
    //   provide: NG_VALIDATORS,
    //   useValue: validate,
    //   multi: true
    // }
  ]
})

export class PasswordInputComponent extends InputBaseComponent {
  public type: string = 'password'
  public label: string = 'Enter your password'
  public placeholder: string = 'Password'

  constructor(injector: Injector) { super(injector) }
}
