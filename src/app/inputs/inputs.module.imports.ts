import { CommonModule } from '@angular/common'

import { FormsModule } from '@angular/forms'

import { NgbModule } from '@ng-bootstrap/ng-bootstrap'


export const IMPORTS = [
  CommonModule,
  FormsModule,

  NgbModule
]
