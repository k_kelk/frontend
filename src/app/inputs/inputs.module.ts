import { NgModule } from '@angular/core'

import { IMPORTS }      from './inputs.module.imports'
import { DECLARATIONS } from './inputs.module.declarations'

@NgModule({
  imports:      IMPORTS,
  declarations: DECLARATIONS,
  exports:      DECLARATIONS
})

export class InputsModule {}
