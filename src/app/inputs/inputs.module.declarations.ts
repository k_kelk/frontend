import { EmailInputComponent } from './email-input'
import { PasswordInputComponent } from './password-input'


export const DECLARATIONS = [
  EmailInputComponent,
  PasswordInputComponent
]
