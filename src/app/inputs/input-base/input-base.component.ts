import { Injector, Input, OnInit, EventEmitter, Output } from '@angular/core'
import { ControlValueAccessor, NgControl } from '@angular/forms'

const noop = () => {
}
export class InputBaseComponent implements ControlValueAccessor, OnInit {
  public inspectable: boolean = true
  public type: string = 'text'
  public tips: boolean = true
  public maxLength
  public control: any

  public placeholder: string
  public name: string

  public _focused: any

  @Input() label: string
  @Input() disabled: boolean = false
  @Output() events = new EventEmitter()
  protected innerValue: any = ''

  public onTouchedCallback: () => void = noop
  public onChangeCallback = ((args: any) => noop)

  constructor(protected injector: Injector) {
  }

  ngOnInit() {
    this.control = this.injector.get(NgControl)
  }

  get value(): any {
    return this.innerValue
  };

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v
      this.onChangeCallback(v)
    }
  }

  onFocus(item) {
    this.control.reset(this.value)
  }

  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value
    }
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn
  }

  public isErrorsVisible(): boolean {
    return this.control.touched && this.control.invalid
  }

  public get valid(): boolean {
    return this.control.valid
  }

  onKeyPress(event) {
    this.events.emit(this.eventPayload('keyPressed', event))
  }

  isMaxLength() {
    if (this.value && this.maxLength) {
      return this.value.toString().length < this.maxLength
    }
    return true
  }

  private eventPayload(name, event) { return { type: name, event: event } }

}
