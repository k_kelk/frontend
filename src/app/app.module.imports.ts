import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule, PreloadAllModules } from '@angular/router'
import { ROUTES } from './app.routes'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

export const IMPORTS = [
  BrowserModule,
  FormsModule,
  HttpModule,
  RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
  NgbModule.forRoot()
]
