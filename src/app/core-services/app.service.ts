import { Injectable } from '@angular/core'
import { StorageProvider } from './storage.service'
import { Router } from '@angular/router'

export type InternalStateType = {
  [key: string]: any
};

@Injectable()
export class AppState {

	constructor() {}
}
