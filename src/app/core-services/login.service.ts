import { Injectable, EventEmitter } from '@angular/core'
import { APP_CONFIG } from 'app/constants/app'
import { Http, Headers, RequestOptions } from '@angular/http'
import { StorageProvider } from './storage.service'
import { Router } from '@angular/router'


@Injectable()
export class LoginService {
	/* TODO: Create user interface instead of type: any */
	public userData: any = {}
	public userLoggedIn: boolean = false
	public userEvents = new EventEmitter()

  constructor(
  	private http: Http,
  	private store: StorageProvider,
  	private router: Router
  ) {
  	this.fetchUserData().subscribe(e => {
  		if (e.action === 'navigate') {
  			this.router.navigate(['client/home/dashboard'])
  		}
  	})
  }

  logUserIn(data) {
  	return this.http.post(APP_CONFIG.API_URL + 'login', data, APP_CONFIG.REQUEST_OPTIONS())
  }

  isLoggedIn() {
  	if (this.userData.id && this.store.storage.getItem('app_key')) {
  		return true
  	} else {
  		this.router.navigate(['/login'])
  		return false
  	}
  }

  ngOnInit() {
  	
  }

  fetchUserData() {
  	let request = this.http.get(
  		APP_CONFIG.API_URL + 'api/me',
  		APP_CONFIG.REQUEST_OPTIONS(this.store.storage.getItem('app_key'))
  	)

  	request.subscribe(e => {
  		this.userData = e.json().data
  		this.userLoggedIn = true
  		this.userEvents.emit({
  			action: 'navigate'
  		})
  	})

  	return this.userEvents
  }

  getUserData() {
  	return this.userData
  }

  logoutUser() {
  	this.userLoggedIn = false
		this.userData = {}
		this.store.storage.removeItem('app_key')
		this.router.navigate(['/login'])
	}
}