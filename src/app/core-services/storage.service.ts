import { Injectable } from '@angular/core'
import { Cookie } from 'ng2-cookies/src'

class CookieStorage {
  setItem(key, value, days = 365, path = '/') {
    Cookie.set(key, value, days, path) // MIGRATION
  }

  getItem(key) {
    return Cookie.get(key);
  }

  removeItem(key) {
    Cookie.delete(key);
  }
}

class StorageMock {
  private _storage: Object = {}

  setItem(key, value) {
    this._storage[key] = value
  }

  getItem(key) {
    return this._storage[key]
  }
}

@Injectable()
export class StorageProvider {
  public storage: any
  public sessionStorage: any
  //providers
  public cookieStorage = new CookieStorage()
  public storageMock = new StorageMock()

  constructor() {
    this.storage = this.testStorage( localStorage, this.cookieStorage )
    this.sessionStorage = this.testStorage( sessionStorage, this.storageMock )
  }

  public setItem(key: string, value: string) {
    this.storage.setItem(key, value)
  }
  public getItem(key: string) {
    this.storage.getItem(key)
  }
  public removeItem(key) {
    this.storage.removeItem( key );
  }

  private testStorage(storage, fallback) {
    try {
      storage.setItem('tryStorage', 'tryStorage')
      storage.removeItem('tryStorage')
      return storage
    } catch (e) { return fallback }
  }
}
