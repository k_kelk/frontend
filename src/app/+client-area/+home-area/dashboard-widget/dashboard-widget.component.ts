import { Component } from '@angular/core'

@Component({
  selector: 'dashboard-widget',
  templateUrl: './dashboard-widget.component.pug',
  styleUrls: [ './dashboard-widget.component.scss' ]
})

export class DashboardComponent {}
