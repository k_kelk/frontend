import { DashboardComponent } from './dashboard-widget'
import { UiElementsComponent } from './ui-elements'

export const DECLARATIONS = [
  DashboardComponent,
  UiElementsComponent
]
