import { NgModule } from '@angular/core'

import { IMPORTS }      from './home-area.module.imports'
import { DECLARATIONS } from './home-area.module.declarations'
import { EXPORTS } from './home-area.module.exports'
import { PROVIDERS } from './home-area.providers'

@NgModule({
  imports:      IMPORTS,
  declarations: DECLARATIONS,
  exports:      EXPORTS,
  providers:    PROVIDERS
})

export class HomeAreaModule {}
