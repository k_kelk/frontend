import { RouterModule } from '@angular/router'
import { routes } from './home-area.module.routing'
import { CommonModule } from '@angular/common'

import { ElementsModule } from '../../elements'
import { FormsModule } from '@angular/forms'

export const IMPORTS = [
  ElementsModule,
  RouterModule.forChild(routes),
  CommonModule,
  FormsModule
]
