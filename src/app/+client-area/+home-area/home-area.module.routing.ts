import { DashboardComponent } from './dashboard-widget'
import { UiElementsComponent } from './ui-elements'

export const routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'ui', component: UiElementsComponent }
]
