import { Component } from '@angular/core'

@Component({
  selector: 'ui-elements',
  templateUrl: './ui-elements.component.pug',
  styleUrls: [
    './ui-elements.component.scss'
  ]
})

export class UiElementsComponent {}
