import { ModuleWithProviders } from '@angular/core'
import { RouterModule } from '@angular/router'

import { ClientAreaComponent } from './client-area.component'

export const routes = [
   { path: '', component: ClientAreaComponent, children: [
      { path: 'home', loadChildren: './+home-area#HomeAreaModule' },
      { path: 'account', loadChildren: './+account-area#AccountAreaModule' }
  	]
  }
]
