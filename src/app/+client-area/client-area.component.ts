import { Component } from '@angular/core'

import { Router,  NavigationStart, NavigationEnd } from '@angular/router'
import { ClientAreaService } from './client-area.service'

@Component({
  selector: 'client__dashboard',
  templateUrl: 'client-area.component.pug',
  styleUrls: ['client-area.component.scss']
})

export class ClientAreaComponent {
  constructor(
    private router: Router,
    private clientAreaService: ClientAreaService
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd ) {
        this.clientAreaService.route_change.end.emit(event)
      }
      if (event instanceof NavigationStart ) {
        this.clientAreaService.route_change.start.emit(event)
      }
    })
  }
}
