import { LAYOUT_DECLARATIONS } from './layout'
import { ClientAreaComponent } from './client-area.component'

export const DECLARATIONS = [
  ClientAreaComponent,

  ...LAYOUT_DECLARATIONS
]
