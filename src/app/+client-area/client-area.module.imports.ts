import { RouterModule } from '@angular/router'
import { routes } from './client-area.module.routing'
import { CommonModule } from '@angular/common'


export const IMPORTS = [
  RouterModule.forChild(routes),
  CommonModule
]
