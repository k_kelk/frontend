import { Component } from '@angular/core'
import { ClientAreaService } from '../../../client-area.service'

@Component({
  selector: 'layout-c__breadcrumb',
  templateUrl: './breadcrumb.component.pug',
  styleUrls: [
    './breadcrumb.component.scss'
  ]
})

export class BreadCrumbComponent {
  public crumbs = ['', '']

  constructor(public clientService: ClientAreaService) {
    this.clientService.route_change.end.subscribe((event) => {
      this.crumbs = [
        event.url.split('/')[2],
        event.url.split('/')[3]
      ]
    })
  }
}
