import { ContentWrapperComponent } from './content-wrapper.component'
import { BreadCrumbComponent } from './breadcrumb'

export const CONTENT_WRAPPER_DECLARATIONS = [
  ContentWrapperComponent,
  BreadCrumbComponent
]
