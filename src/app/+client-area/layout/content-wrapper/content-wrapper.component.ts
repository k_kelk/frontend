import { Component, HostBinding } from '@angular/core'
import { ClientAreaService } from '../../client-area.service'

@Component({
  selector: 'layout__content-wrapper',
  templateUrl: './content-wrapper.component.pug',
  styleUrls: ['./content-wrapper.component.scss']
})
export class ContentWrapperComponent {
  constructor(public clientAreaService: ClientAreaService) {}

  @HostBinding('class.sidebar-closed') get sidebar_status() {
    return !this.clientAreaService.sidebar_opened
  }

}
