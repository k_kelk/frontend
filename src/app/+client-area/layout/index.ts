import { NAVIGATIONBAR_DECLARATIONS } from './navigation-bar'
import { SIDEBAR_DECLARATIONS } from './side-bar'
import { CONTENT_WRAPPER_DECLARATIONS } from './content-wrapper'


export const LAYOUT_DECLARATIONS = [
   ...NAVIGATIONBAR_DECLARATIONS,
   ...SIDEBAR_DECLARATIONS,
   ...CONTENT_WRAPPER_DECLARATIONS

]
