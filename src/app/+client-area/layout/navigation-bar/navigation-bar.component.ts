import { Component } from '@angular/core'
import { ClientAreaService } from '../../client-area.service'

@Component({
  selector: 'layout__navigation-bar',
  templateUrl: 'navigation-bar.component.pug',
  styleUrls: ['navigation-bar.component.scss']
})
export class NavigationBarComponent {
  constructor(public clientAreaService: ClientAreaService) {}
  toggleSidebar() {
    this.clientAreaService.sidebar_opened = !this.clientAreaService.sidebar_opened
  }
}
