import { Component } from '@angular/core'
import { SideBarService } from '../side-bar.service'

@Component({
  selector: 'layout__side-bar-nav',
  templateUrl: './side-bar-nav.component.pug',
  styleUrls: [
    './side-bar-nav.component.scss'
  ]
})

export class SideBarNavComponent {
  public nav_config = []

  constructor(public sbs: SideBarService) {
    this.sbs.items.subscribe(data => {
      this.nav_config = data.items
    })
  }
}
