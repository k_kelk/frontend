import { SideBarNavComponent } from './side-bar-nav.component'
import { DropdownComponent } from './dropdown'
import { ItemComponent } from './item'


export const SIDE_BAR_NAV_DECLARATIONS = [
  SideBarNavComponent,
  DropdownComponent,
  ItemComponent
]
