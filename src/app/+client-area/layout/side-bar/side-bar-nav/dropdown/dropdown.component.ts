import { Component, Input, HostBinding } from '@angular/core'

@Component({
  selector: 'nav__dropdown',
  templateUrl: './dropdown.component.pug',
  styleUrls: [
    './dropdown.component.scss',
    '../item/item.component.scss'
  ]
})

export class DropdownComponent {
  public hidden: boolean

  @Input() title: string
  @Input() icon: string

  @HostBinding('class.open') open: boolean = false
}
