import { Component, Input } from '@angular/core'

@Component({
  selector: 'nav__item',
  templateUrl: './item.component.pug',
  styleUrls: [
    './item.component.scss'
  ]
})

export class ItemComponent {
  @Input() icon: string
  @Input() path: string
}
