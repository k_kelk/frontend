import { Injectable, EventEmitter } from '@angular/core'
import { SIDEBAR_ITEMS } from 'app/constants/layout'

@Injectable()
export class SideBarService {
  public items = new EventEmitter()

  constructor() {
    this.fetchSidebarItems()
  }

  fetchSidebarItems() {
    setTimeout(()=>{
      this.items.emit({
        items: SIDEBAR_ITEMS
      })
    }, 100)
  }
}
