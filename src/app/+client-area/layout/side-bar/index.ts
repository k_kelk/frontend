import { SideBarComponent } from './side-bar.component'
import { SideBarHeaderComponent } from './side-bar-header'
import { SIDE_BAR_NAV_DECLARATIONS } from './side-bar-nav'

export const SIDEBAR_DECLARATIONS = [
  SideBarComponent,
  SideBarHeaderComponent,
  ...SIDE_BAR_NAV_DECLARATIONS
]
