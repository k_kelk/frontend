import { Component, HostBinding } from '@angular/core'
import { ClientAreaService } from '../../client-area.service'
import { SideBarService } from './side-bar.service'

@Component({
  selector: 'layout__side-bar',
  templateUrl: './side-bar.component.pug',
  styleUrls: [ './side-bar.component.scss' ],
  providers: [ SideBarService ]
})

export class SideBarComponent {
  constructor( private clientAreaService: ClientAreaService, private sbs: SideBarService ) {}
  @HostBinding('class.closed') get closed() {
    return !this.clientAreaService.sidebar_opened
  }
}
