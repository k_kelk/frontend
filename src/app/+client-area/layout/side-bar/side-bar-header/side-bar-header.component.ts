import { Component, HostBinding } from '@angular/core'
import { AppState, LoginService } from '../../../../core-services'

@Component({
  selector: 'layout__side-bar-header',
  templateUrl: './side-bar-header.component.pug',
  styleUrls: [
    './side-bar-header.component.scss'
  ]
})

export class SideBarHeaderComponent {
  @HostBinding('class.fadeIn') loading: boolean = true
  
  
  constructor(public loginService: LoginService) {
  	console.log(this.loginService.userData.profile_img_url)
  }

  onImageLoaded() {
    this.loading = true;
  }
}
