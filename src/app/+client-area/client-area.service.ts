import { Injectable, EventEmitter } from '@angular/core'
import { Router } from '@angular/router'

@Injectable()
export class ClientAreaService {
  public sidebar_opened: boolean = true

  public route_change = {
    'start': new EventEmitter(),
    'end': new EventEmitter()
  }

  public sidebar_items = [
    { type: 'title', content: 'Home' },
    { type: 'item', icon: 'speedometer', content: 'Dashboard', link: '/client/home/dashboard' },
    { type: 'item', icon: 'Thumnails', content: 'Dashboard', link: '/client/home/thumbnails' },
    { type: 'dropdown', icon: 'briefcase', title: 'Briefcase', items: [
        { type: 'item', icon: 'book-open', content: 'Elements', link: '/client/home/ui' },
      ]
    },

    { type: 'title', content: 'Account' },
    { type: 'item', icon: 'settings', content: 'Settings', link: '/client/account/settings' },

    { type: 'title', content: 'Common' },
    { type: 'item', icon: 'logout', content: 'Logout', link: '/login' },
  ]
}
