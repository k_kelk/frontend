import { NgModule } from '@angular/core'

import { IMPORTS }      from './client-area.module.imports'
import { DECLARATIONS } from './client-area.module.declarations'
import { EXPORTS } from './client-area.module.exports'
import { PROVIDERS } from './client-area.module.providers'

@NgModule({
  imports:      IMPORTS,
  declarations: DECLARATIONS,
  exports:      EXPORTS,
  providers:    PROVIDERS
})

export class ClientAreaModule {}
