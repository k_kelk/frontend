import { SettingsWidgetComponent } from './settings-widget'

export const routes = [
  { path: 'settings', component: SettingsWidgetComponent }
]
