import { NgModule } from '@angular/core'

import { IMPORTS }      from './account-area.module.imports'
import { DECLARATIONS } from './account-area.module.declarations'
import { EXPORTS } from './account-area.module.exports'

@NgModule({
  imports:      IMPORTS,
  declarations: DECLARATIONS,
  exports:      EXPORTS
})

export class AccountAreaModule {}
