import { Component } from '@angular/core'

@Component({
  selector: 'settings-widget',
  templateUrl: './settings-widget.component.pug',
  styleUrls: [
    './settings-widget.component.scss'
  ]
})

export class SettingsWidgetComponent {
}
