import { RouterModule } from '@angular/router'
import { routes } from './account-area.module.routing'

export const IMPORTS = [
  RouterModule.forChild(routes)
]
