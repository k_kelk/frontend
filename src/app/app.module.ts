import { NgModule, ApplicationRef } from '@angular/core'
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr'

// App is our top level component
import { AppComponent } from './app.component'

import { DECLARATIONS } from './app.module.declarations'
import { IMPORTS } from './app.module.imports'
import { PROVIDERS } from './app.module.providers'




import { AppState, InternalStateType } from './core-services'

import '../styles/styles.scss'
import '../styles/headings.css'


type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
}

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: DECLARATIONS,
  imports: IMPORTS,
  providers: PROVIDERS
})
export class AppModule {

  constructor(
    public appRef: ApplicationRef,
    public appState: AppState
  ) {}

}
