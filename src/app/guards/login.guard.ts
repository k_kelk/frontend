import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { LoginService } from '../core-services';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(
  	private loginService: LoginService
  ) {}

  canActivate() {
    return this.loginService.isLoggedIn();
  }
}