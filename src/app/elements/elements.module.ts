import { NgModule } from '@angular/core'

import { IMPORTS }      from './elements.module.imports'
import { DECLARATIONS } from './elements.module.declarations'
import { EXPORTS } from './elements.module.exports'

@NgModule({
  imports:      IMPORTS,
  declarations: DECLARATIONS,
  exports:      DECLARATIONS
})

export class ElementsModule {}
