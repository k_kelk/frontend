import { Component, Input } from '@angular/core'

@Component({
  selector: 'elem__card',
  templateUrl: './card.component.pug',
  styleUrls: [
    './card.component.scss'
  ]
})

export class CardComponent {
  @Input() title: string = ''
  @Input() subtitle: string = ''
}
