import { CardComponent } from './card'
import { TabbedViewComponent } from './tabbed-view'

export const DECLARATIONS = [
  CardComponent,
  TabbedViewComponent
]
