import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
  selector: 'elem__tabbed-view',
  templateUrl: './tabbed-view.component.pug',
  styleUrls: [
    './tabbed-view.component.scss'
  ]
})

export class TabbedViewComponent {
  @Input() tabs: any
  @Output() tabchange = new EventEmitter()
  public active: number = 0

  onClick(i) {
    this.active = i
    console.log("tab is now: " + this.active)
    this.tabchange.emit(this.active)
  }
}
