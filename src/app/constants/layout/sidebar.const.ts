export const SIDEBAR_ITEMS = [
	{
    "section_title": "Main",
    "section_items": [
      {
        "type": "nav_link",
        "path": "/client/home/dashboard",
        "icon_class": "speedometer",
        "link_name": "Dashboard"
      }
    ]
  },
  {
    "section_title": "Machine Logs",
    "section_items": [
      {
        "type": "nav_link",
        "path": "/client/home/ui",
        "icon_class": "screen-desktop",
        "link_name": "machine-01-TLN"
      },
      {
        "type": "nav_link",
        "path": "/client/home/ui",
        "icon_class": "screen-desktop",
        "link_name": "machine-02-TLN"
      }
    ]
  }
]
