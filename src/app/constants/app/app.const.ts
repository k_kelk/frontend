import { Headers, RequestOptions } from '@angular/http'



export const APP_CONFIG = {
	API_URL: 'http://app.dev/',

	REQUEST_OPTIONS: function(auth) {
		let headers = new Headers();
    		headers.append('Content-Type', 'application/json');
    
    if (auth) headers.append('Authorization', auth);

    let options = new RequestOptions({ 
      headers: headers, 
      withCredentials: true
    })
    console.log(options)
    return options;
	}

}