import { Routes } from '@angular/router'
import { HomeComponent } from './home'
import { DataResolver } from './app.resolver'
import { LoginGuard } from './guards'

export const ROUTES: Routes = [
  { 
  	path: 'login',
  	loadChildren: './+public-area#PublicAreaModule'
  },
  { 
  	path: 'client',
  	loadChildren: './+client-area#ClientAreaModule',
  	canActivate: [LoginGuard]
  },
  { path: '**',    redirectTo: 'login' }
]



//import { AboutComponent } from './about'
//import { NoContentComponent } from './no-content'

// { path: '',      component: HomeComponent },
// { path: 'home',  component: HomeComponent },

// { path: 'detail', loadChildren: './+detail#DetailModule'},
// { path: 'barrel', loadChildren: './+barrel#BarrelModule'},
