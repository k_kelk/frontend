import { Component, ViewEncapsulation } from '@angular/core';
import { AppState } from './core-services';

@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [ './app.component.scss' ],
  templateUrl: './app.component.pug'
})

export class AppComponent {
  constructor(
    public appState: AppState
  ) {}
}
