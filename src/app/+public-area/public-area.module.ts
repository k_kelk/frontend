import { NgModule } from '@angular/core'

import { IMPORTS }      from './public-area.module.imports'
import { DECLARATIONS } from './public-area.module.declarations'

import { PROVIDERS }    from './public-area.module.providers'


@NgModule({
  imports: IMPORTS,
  declarations: DECLARATIONS,
  providers: PROVIDERS
})

export class PublicAreaModule { }
