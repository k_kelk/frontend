import { ModuleWithProviders } from '@angular/core'
import { RouterModule } from '@angular/router'

import { LOGIN_PAGE_ROUTES } from './login-page'


export const routes = [
   { path: '', children: [
      ...LOGIN_PAGE_ROUTES,
  ]}
]
