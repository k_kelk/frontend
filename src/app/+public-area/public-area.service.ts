import { Injectable } from '@angular/core'
import { LoginService } from '../core-services'

@Injectable()
export class PublicAreaService {
  constructor(
    private loginService: LoginService
  ) {}

  login(data) { return this.loginService.logUserIn(data) }

  fetchUserData() { return this.loginService.fetchUserData() }

  /* TODO: HANDLE HTTP ERRORS ON APP LEVEL, BUT NOT IN THIS TEST TASK */
  handleHTTPErrors(status) {
  	switch (status) {
  		case 404:
  			console.warn("NOT FOUND 404")
  			break;
  		
  		default:
  			alert("UNHANDLED ERROR: " + status)
  			break;
  	}
  }
}