import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { HttpModule }   from '@angular/http'
import { FormsModule } from '@angular/forms'
import { InputsModule } from '../inputs'

import { routes } from './public-area.module.routing'

export const IMPORTS = [
  CommonModule,
  RouterModule.forChild(routes),
  HttpModule,
  FormsModule,
  InputsModule,

  //PublicAreaModuleRouting
]
