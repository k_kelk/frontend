import { LoginPageComponent } from './login-page.component'

export const LOGIN_PAGE_DECLARATIONS = [
   LoginPageComponent
]

export const LOGIN_PAGE_ROUTES = [
  { path: '', component:  LoginPageComponent}
]
