import { Component, ViewChild, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { NgForm } from '@angular/forms'
import { PublicAreaService } from '../public-area.service'
import { StorageProvider } from '../../core-services'

@Component({
  selector: 'public__login',
  templateUrl: 'login-page.component.pug',
  styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent implements OnInit {
  @ViewChild('form') public form: NgForm

  public formModel: any = {
    email: 'kaarelkelk@gmail.com',
    password: 'secret'
  }

  public errors:any = {}


  constructor(
    private router: Router,
    private pas: PublicAreaService,
    public store: StorageProvider
  ) {}

  onFormSubmit() {
    this.pas.login(this.formModel)
        .subscribe(e => {
          this.store.storage.setItem('app_key', e.json().api_key)
          this.pas.fetchUserData().subscribe(e => {
            this.navigateToApp()
          }) 
        }, e => {
          // if status is 422/401 use form error handler otherwise app level error 
          e.status === 422 || e.status === 401 ? 
            this.onLoginError(e.json()) : 
            this.pas.handleHTTPErrors(e.status);
        })
    return false;
  }

  ngOnInit() {
    this.isLoggedIn();
  }
  
  isLoggedIn() {
    if (this.store.storage.getItem('app_key')) this.navigateToApp();
  }

  navigateToApp() {
    this.router.navigate(['client/home/dashboard'])
  }

  onLoginError(e) {
    // console.warn(e)
    this.errors = e;
  }
}
